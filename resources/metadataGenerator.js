var metadataGenerator=(function (){
	var requiredIndicator=0;
	var init=function(){
		requiredIndicator=0;
		metadataGenerator.binePageEvents();
		metadataGenerator.loadLabels();
		metadataGenerator.loadPlaceHolders();
		metadataGenerator.applyLicenceColor();
	};
	var applyLicenceColor=function(){
		if($("select[title='licence']").val()==="CC-0" || $("select[title='licence']").val()==="CC-BY"){
		    	$("select[title='licence']").addClass( "light-green" );
		    }
		    else{
		    	$("select[title='licence']").removeClass( "light-green" );
		    }
		};
	var binePageEvents=function(){
		$(".today").datepicker();
		$(".today" ).datepicker( "setDate", new Date() );
		$("select[title='educationalInstitution']").val(dictionary_metadata.defaultValues.educationalInstitution);
		$("select[title='licence']").val(dictionary_metadata.defaultValues.licence);
		$(".help").off().on("click",function(e){
				$.alert({
				    title: dictionary_metadata.labels.helpHeading,
				    content: dictionary_metadata.helpValues[$(this).attr("title")],
				    theme:"Supervan",
				    columnClass: "small"
				});
				var a=$(this).title;
		});
	$("select[title='licence']").val()
		$("#institutionDpd").off().on("change",function(e){
			var idx=dictionary_metadata.optionValues.institution.indexOf($(this).val());
			$("#institutionRor").val(dictionary_metadata.optionValues.institutionRor[idx]);
		});
		$(".licence_check").off().on("change",function(e){
			var val=$(this).is(":checked");
			var licenseShortName = "CC";
		    if ($("#check_BY").is(":checked")== false) {
		      licenseShortName += "-" + $("#check_BY").val();
		    }
		    if ($("#check_SA").is(":checked") == false) {
		      licenseShortName += "-" + $("#check_SA").val();
		    }
		    if ($("#check_ND").is(":checked") == false) {
		      licenseShortName += "-" + $("#check_ND").val();
		    }
		    if ($("#check_NC").is(":checked")== false) {
		      licenseShortName += "-" + $("#check_NC").val();
		    }
		    if (licenseShortName == "CC") {
		    	licenseShortName="CC-0";
		    }
		    $("select[title='licence']").val(licenseShortName);
		    metadataGenerator.applyLicenceColor();
		});
	};
	var loadLabels=function(){
		var labelDict=dictionary_metadata.labels;
		for(label in dictionary_metadata.labels){
			$("#"+label).text(dictionary_metadata.labels[label]);
		}
	};
	var loadPlaceHolders=function(){
		return false;
	};
	return{
		init:init,
		binePageEvents:binePageEvents,
		requiredIndicator:requiredIndicator,
		loadLabels:loadLabels,
		loadPlaceHolders:loadPlaceHolders,
		applyLicenceColor:applyLicenceColor
	}
})();

$(document).ready(function(){
	metadataGenerator.init()
	});
